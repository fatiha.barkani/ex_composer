<?php

//namespace

namespace ISL\Entity;

//create class Personne
class Personne{
    public $id;
    public $nom;
    public $prenom;
    public $adresse;
    public $cp;
    public $pays;
    public $societe;

    //construct function
    public function __construct($nom, $prenom, $adresse, $cp, $pays, $societe){

        $this -> setNom($nom);
        $this -> setPrenom($prenom);
        $this -> setAdresse($adresse);
        $this -> setCp($cp);
        $this -> setPays($pays);
        $this -> setSociete($societe);
    }

    //getter

    public function getId(){
        return $this -> id;
    }

    public function getNom(){
        return $this -> nom;
    }

    public function getPrenom(){
        return $this -> prenom;
    }

    public function getAdresse(){
        return $this -> adresse;
    }

    public function getCp(){
        return $this -> cp;
    }

    public function getPays(){
        return $this -> pays;
    }

    public function getSociete(){
        return $this -> societe;
    }

    //setter

    public function setId($id){
        $this -> id = $id;
    }

    public function setNom($nom){
        $this -> nom = $nom;
    }

    public function setPrenom($prenom){
        $this -> prenom = $prenom;
    }

    public function setAdresse($adresse){
        $this -> adresse = $adresse;
    }

    public function setCp($cp){
        $this -> cp = $cp;
    }

    public function setPays($pays){
        $this -> pays = $pays;
    }

    public function setSociete($societe){
        $this -> societe= $societe;
    }




}
?>