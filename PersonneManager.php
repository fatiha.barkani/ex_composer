<?php


//namespace
namespace ISL\Manager;

//utilisation de la librairie de faker
use Faker\Factory;

use ISL\Entity\Personne;

//connection

use PDO;



//create class personneManager

class PersonneManager{

    function getDbConnexion(){

        $connexion = new PDO('mysql:host=localhost;dbname=poo_php', 'root', '');
        return $connexion;
    }


    static function create($nombre){
        $faker = Factory::create($nombre);
        $fakePers =[];

                
        for($i=0; $i<$nombre; $i++){
            $fakePers [] = new Personne(
                
                $faker ->lastname,
                $faker ->firstname,
                $faker ->address,
                $faker ->postcode,
                $faker ->country,
                $faker ->company,
                
                );

        }
        return $fakePers;
       
    }

    //fonction create personne 

    public function createPers(Personne $objet){
        //query
        $sql = "INSERT INTO personnes (nom, prenom, adresse, cp, pays, societe) VALUES(?,?,?,?,?,?)";

        //statement prepare
        $stmt = $this->getDbConnexion()->prepare($sql);

        //execute
        $stmt->execute(array($objet->getNom(), $objet->getPrenom(), $objet->getAdresse(), $objet ->getCp(), $objet ->getPays(), $objet ->getSociete()));
    }

    //fonction readAll

    public function readAll(){
        //query
        $sql="SELECT * FROM 'personnes'";

        //statement prepare
        $stmt=$this->getDbConnexion()->prepare($sql);

        //execute
        $stmt->execute();
        $results = $stmt->fetchAll();
        $personne = array();

        foreach($results as $result){
            $personne = new Personne($result["id"], $result["nom"], $result["prenom"], $result["adresse"], $result["cp"], $result["pays"], $result["societe"]);
            $personnes[] = $personne;
        }
        return $personne;
    }


    //fonction read

    public function read ($id){
        //query
        $sql = "SELECT * FROM 'personnes' WHERE id=$id";

        //statement prepare
        $stmt = $this -> getDbConnexion() -> prepare($sql);

        //execute
        $stmt -> execute();
        $result = $stmt -> fetch();
        //$personne = new Personne($result["id"], $result["nom"], $result["prenom"], $result["adresse"], $result["cp"], $result["pays"], $result["societe"]);
        return $result;
    }


    //fonction update

    public function update(Personne $objet){

        //query
        $sql="UPDATE personnes
              SET nom= '".$objet->getNom()."',
                  prenom= '".$objet->getPrenom()."',
                  adresse= '".$objet->getAdresse()."',
                  cp= '".$objet->getCp()."',
                  pays= '".$objet->getPays()."',
                  societe= '".$objet->getSociete()."'
                
              WHERE  id= '".$objet->getId()."'";
              var_dump($sql);


        //statement prepare
        $stmt = $this->getDbConnexion()->prepare($sql);

        //execute
        $stmt->execute();

    }

    //fonction delete

    public function delete(Personne $objet){

        //query
        $sql = "DELETE FROM personnes WHERE id= " .$objet->getId();
    
        //statement prepare
        $stmt = $this->getDbConnexion()->prepare($sql);
    
        //execute
        return $stmt->execute();
        
    }

    //fonction destruct

    public function __destruct(){
        echo "je suis une personne en destruction";
    }


    //fonction to string
    public function __toString(){
       return "je suis une personne ";
    }
    

}

?>